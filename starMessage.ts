import { Message, MessageEmbed, MessageAttachment } from "discord.js";
import Bot from "./src/bot";

export default class StarMessage {
  private bot: Bot;

  private msg: Message;
  private embed = new MessageEmbed();
  private attachments?: MessageAttachment[];

  constructor(bot: Bot, target: Message) {
    this.bot = bot;

    this.embed
      .setAuthor(target.author.username, target.author.avatarURL())
      .setDescription(target.content)
      .setTimestamp(target.editedAt ?? target.createdAt)
      .setFooter(`1 ${bot.emoji}`);
    const attachments: MessageAttachment[] = (target.attachments as any).array();

    if (attachments.length === 1) this.embed.setImage(attachments[0].url);
    else if (attachments.length > 1) this.attachments = attachments;
  }

  async create() {
    this.msg = await this.bot.channel.send(this.embed);
    if (this.attachments) {
      await this.bot.channel.send("attachments:", { files: this.attachments });
    }
    return this;
  }

  update(stars: number): boolean {
    if (stars === 0) {
      this.msg.delete();
      return true;
    } else {
      this.embed.setFooter(`${stars} ${this.bot.emoji}`);
      this.msg.edit(this.embed);
    }
  }
}
