import { Command } from "./command";
import { TextChannel } from "discord.js";

const commands: Command[] = [
  {
    triggers: ["channel"],
    description: "sets the channel to send starred messages to",
    run: (context) => {
      const channels: TextChannel[] = (context.msg.mentions
        .channels as any).array();
      if (channels.length === 1) {
        context.bot.channel = channels[0];
        context.msg.channel.send(`Channel set to "${channels[0].name}"`);
        context.bot.save();
      } else {
        context.msg.channel.send("Please enter one valid channel.");
      }
    },
  },
  {
    triggers: ["prefix"],
    description: "sets the prefix",
    run: (context) => {
      if (context.content.length > 0) {
        context.bot.prefix = context.content;
        context.msg.channel.send(`Prefix set to ${context.content}`);
        context.bot.save();
      } else {
        context.msg.channel.send("Please specify a prefix");
      }
    },
  },
  {
    triggers: ["emoji"],
    description: "sets the emoji that the bot looks for and counts",
    run: (context) => {
      if (context.content.length > 0) {
        context.bot.emoji = context.content;
        context.msg.channel.send(`Emoji set to ${context.content}`);
        context.bot.save();
      } else {
        context.msg.channel.send("Please specify an emoji");
      }
    },
  },
  {
    triggers: ["help"],
    description: "displays all commands and some information about them",
    run: (context) => {
      context.msg.channel.send(
        commands
          .map((c) => `⭐ ${c.triggers.join(", ")} - ${c.description}`)
          .join("\n")
      );
    },
  },
];

export default commands;
