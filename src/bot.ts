import { writeFile } from "fs";

import { Client, TextChannel, MessageReaction, Message } from "discord.js";

import commands from "./commands";
import StarMessage from "../starMessage";

export default class Bot {
  readonly client = new Client();

  public prefix = "s!";
  public emoji = "⭐";
  public channel?: TextChannel = null;
  public channelMap: Map<string, StarMessage> = new Map();
  public dirty: { msg: Message; count: number }[] = [];

  constructor() {
    this.client.on("message", (msg) => {
      let content = msg.content;
      if (content.startsWith(this.prefix)) {
        content = content.slice(this.prefix.length);
        for (let command of commands) {
          if (command.triggers.some((t) => content.startsWith(t))) {
            command.run({
              content: content.slice(
                content.indexOf(" ") + 1 ?? 0,
                content.length
              ),
              bot: this,
              msg,
            });
            break;
          }
        }
      }
    });

    const update = (reaction: MessageReaction) => {
      if (
        reaction.message.channel instanceof TextChannel &&
        reaction.emoji.name === this.emoji
      ) {
        const d = this.dirty.find((d) => d.msg.id === reaction.message.id);
        let count = d?.count ?? reaction.count;
        if (d) d.count = count;
        else {
          this.dirty.push({ msg: reaction.message, count });
        }
      }
    };

    this.client.on("messageReactionAdd", update);
    this.client.on("messageReactionRemove", update);
    this.client.on("messageReactionRemoveAll", (msg) => {
      this.channelMap.get(msg.id)?.update(0);
    });
    this.client.on("messageReactionRemoveEmoji", update);

    this.client.on("ready", async () => {
      console.log("connected");
      await this.load();
      console.log("loaded");
      setInterval(this.tick.bind(this), 1000);
    });
  }

  async load() {
    try {
      const data = require("../data.json");
      if (data.prefix) this.prefix = data.prefix;
      if (data.emoji) this.emoji = data.emoji;
      const channel = await this.client.channels.fetch(data.channel);
      if (channel && channel instanceof TextChannel) {
        this.channel = channel;
      }
    } catch (err) {}
  }

  async start() {
    const config = require("../config.json");
    await this.client.login(config.token);
  }

  save() {
    return new Promise<void>((resolve) => {
      writeFile(
        "./data.json",
        JSON.stringify({
          emoji: this.emoji,
          prefix: this.prefix,
          channel: this.channel.id,
        }),
        { encoding: "utf-8" },
        () => {
          resolve();
        }
      );
    });
  }

  tick() {
    this.dirty.forEach(async (d) => {
      this.updateReaction(d.msg, d.count);
    });
    this.dirty = [];
  }

  async updateReaction(msg: Message, count: number) {
    if (!this.channelMap.has(msg.id)) {
      this.channelMap.set(msg.id, await new StarMessage(this, msg).create());
    } else {
      if (this.channelMap.get(msg.id).update(count)) {
        this.channelMap.delete(msg.id);
      }
    }
  }
}
