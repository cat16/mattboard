import Bot from "./bot";
import { Message } from "discord.js";

export interface Command {
  triggers: string[];
  description: string;
  run: (context: { content: string; msg: Message; bot: Bot }) => void;
}
